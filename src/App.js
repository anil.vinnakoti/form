import React, {useEffect, useReducer} from 'react';
import './App.css'
import Form from './components/Form';
import Practice from './components/practice/Practice';


export const UserContext = React.createContext()

const initialState = {
  userDetails:{
    firstName:"",
    lastName:"",
    email:"",
    address:"",
    gender:"",
    checkbox:false
  }
}

const reducer = (state, action) => {
  switch (action.type){
    case "CHANGE_USER": return{
      ...state,
      userDetails : action.payload
    }
    default: return state

  }
}

function App() {

  const [userData, dispatch] = useReducer(reducer, initialState)

  console.log(userData);

  return<div>
    <UserContext.Provider value={{userState:userData, userDispatch:dispatch}}>
      <Form />
      {/* <Practice /> */}
    </UserContext.Provider>

  </div>
}

export default App;
