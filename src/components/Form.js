import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { UserContext } from "../App";

function Form() {

  const userContext = useContext(UserContext)

  const {userState, userDispatch} = userContext

  const { register, handleSubmit, formState:{errors} } = useForm();

  const submitHandler = data => {
    userDispatch({type:"CHANGE_USER", payload:data})
  }
  

  return <form onSubmit={handleSubmit(submitHandler)} className="form-container">

      <div className="field">
        <label>First Name</label>
        <input {...register("firstName", {
          required:"First Name is required *",
          minLength:{
            value:3,
            message:"Must be 3 characters"
          }
        })} placeholder="Enter first name" type="text" />
        <p style={{color:"red"}}>{errors.firstName?.message}</p>
      </div>

      <div className="field">
        <label>Last Name</label>
        <input {...register("lastName", {
          required:"Last Name is required *"
        })} placeholder="Enter last name" type="text" />
        <p style={{color:"red"}}>{errors.lastName?.message}</p>
      </div>

      <div className="field">
        <label>Email</label>
        <input {...register('email', {
          required:"Email id is required",pattern: {
            value: /\S+@\S+\.\S+/,
            message: 'Email is not valid.'
          }
        })} placeholder="Enter email"  />
        <p style={{color:'red'}}>{errors.email?.message}</p>
      </div>

      <div className="field">
        <label>Address</label>
        <input {...register("address", {
          required:"Address is required"
        })} placeholder="Enter address" type="text" />
        <p style={{color:'red'}}>{errors.address?.message}</p>
      </div>

      <div className="field">
        <label>Password</label>
      <input type="password" 
      placeholder="Enter password"
      {...register("password",{
        required:"Password is required",
        minLength:{
          value:8,
          message:"password length must be 8 characters"
        },
        validate:(value) => {
          return (
            [/[a-z]/, /[A-Z]/, /[0-9]/, /[^a-zA-Z0-9]/].every((pattern) =>
              pattern.test(value)
            ) || "must include lower, upper, number, and special chars"
          );
        },
      })}/>
      <p style={{color:'red'}}>{errors.password?.message}</p>
      </div>

      <div className="field radio">
        <label >Gender</label>
        <div className="gender">
          <div>
            <input {...register("gender", {
              required:"Select gender"
            })} type="radio" value="male"/>
            <label >Male</label>
          </div>
          <div>
            <input {...register("gender",{
              required:"Select gender"
            })} type="radio" value="female"/>
            <label >Female</label>
          </div>
        </div>
        <p style={{color:'red'}}>{errors.gender?.message}</p>
      </div>
      <div className="field check">
        <div>
          <input {...register("checkbox",{
            required:"Please read & accept the terms"
          })} type="checkbox" className="check" />
          <span className="terms" >I have read the terms &amp; conditions.</span>
          <p style={{color:'red'}}>{errors.checkbox?.message}</p>
        </div>  
      </div>
      <div className="submit-button">
        <button type="submit" >Submit</button>
      </div>
    </form>;
}

export default Form