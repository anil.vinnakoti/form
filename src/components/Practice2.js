import React, {useEffect} from 'react'
import {useForm} from 'react-hook-form'


let count = 0;

function Practice() {
  const {register, handleSubmit, formState:{errors}} = useForm({
    defaultValues:{
      firstName:'',
      lastName:''
    }
  })
  
  useEffect(() => {
    count++
    console.log(`rendered ${count} times`);
  },[count])
  
  console.log(errors);
  return (
    <form onSubmit={handleSubmit(data => console.log(data))}>
      <input {...register("firstName", {required:"This is required"})} placeholder="First Name"/>
      <p>{errors.firstName?.message}</p>
      <input {...register("lastName", {required:"This is required"})} placeholder="Last Name"/>
      <p>{errors.lastName?.message}</p>
      
      <input type="submit" />
    </form>
  )
}

export default Practice