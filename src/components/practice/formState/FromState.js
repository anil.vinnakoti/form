import React from "react";
import { useForm } from "react-hook-form";

function FormState() {

  const {
    register,
    handleSubmit,
    formState: { errors, isDirty, isSubmitting, touchedFields, submitCount },
  } = useForm({
    defaultValues:{
      firstName:'',
      lastName:''
    }
  });


  const onSubmit = (data) => console.log(data);

  // console.log(errors);
  // console.log(isDirty);
  // console.log(isSubmitting);
  // console.log(touchedFields);
  console.log(submitCount);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input {...register("firstName",{
        required:"This is required"
      })} />
      <input {...register("lastName")} />
      <input type="submit" />
    </form>
  );
}

export default FormState