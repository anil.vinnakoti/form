import React, {useEffect} from 'react'
import { useForm } from "react-hook-form";

let count = 0
function Watch() {

  const { register, watch, handleSubmit } = useForm({
    defaultValues:{
      firstName:'',
      lastName:'',
      age:'',
      check:false
    }
  });
  const watchFirstName = watch("firstName");
  const watchAllFields = watch();
  const watchFields = watch(["firstName", "age"]); 

  //using use effect
  useEffect(() => {
    const subscription = watch((data) => console.log(data));
    return () => {
      subscription.unsubscribe();
    }
  }, [watch]);

  const onSubmit = data => console.log(data); 

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input type="checkbox" {...register("check")} />
         
        {watchFirstName==="anil" &&<p>This is temp</p> }
        <input type="text" {...register("firstName", { min: 50 })} />
        <input type="text" {...register("lastName", { min: 50 })} />
        <input type="submit" />
      </form>
    </>

  )
}

export default Watch