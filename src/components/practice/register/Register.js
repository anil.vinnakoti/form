import React, {useEffect} from "react";
import { useForm } from "react-hook-form";


let count=0;

function Required() {
  const { register, handleSubmit, formState:{errors} } = useForm();
  const onSubmit = data => console.log(data);

  const validatePassword = value => {
    if (value.length < 6) {
      return "Password should be at-least 6 characters.";
    }
  };

  useEffect(() => {
    count++
    console.log(`rendered ${count} times`);
  },[count])

  console.log("errors", errors);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input {...register("firstName", { required: "This is required *" })} placeholder="First name" />

      <input {...register("lastName", { minLength: 2 })} placeholder="Last name" />

      <select {...register("category")}>
        <option value="">Select...</option>
        <option value="A">Category A</option>
        <option value="B">Category B</option>
      </select>
      
      <input {...register("checkbox")} type="checkbox" value="A" />
      <input {...register("checkbox")} type="checkbox" value="B" />
      <input {...register("checkbox")} type="checkbox" value="C" />
      
      <input {...register("radio")} type="radio" value="A" />
      <input {...register("radio")} type="radio" value="B" />
      <input {...register("radio")} type="radio" value="C" />

      <input {...register('dummy', {
        onChange:e => console.log(e.target.value)
      })} />

      <input type="password" 
      {...register("password",{
        required:true,
        value:6,
        validate:validatePassword
      })}/>
      <p>{errors.password?.message}</p>

      <input type="submit" />
    </form>
  );
}

export default Required
